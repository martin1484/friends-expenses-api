package com.friends.expenses.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendsExpensesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriendsExpensesApiApplication.class, args);
	}

}
