package com.friends.expenses.api.exception;

public class CalculationErrorException extends Exception {

    public CalculationErrorException() {
    }

    public CalculationErrorException(String message) {
        super(message);
    }

    public CalculationErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalculationErrorException(Throwable cause) {
        super(cause);
    }

    public CalculationErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
