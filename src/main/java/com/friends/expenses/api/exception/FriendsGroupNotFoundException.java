package com.friends.expenses.api.exception;

public class FriendsGroupNotFoundException extends Exception{

    public FriendsGroupNotFoundException() {
    }

    public FriendsGroupNotFoundException(String message) {
        super(message);
    }

    public FriendsGroupNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FriendsGroupNotFoundException(Throwable cause) {
        super(cause);
    }

    public FriendsGroupNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
