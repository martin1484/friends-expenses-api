package com.friends.expenses.api.repository;

import com.friends.expenses.api.entity.Expense;
import com.friends.expenses.api.entity.FriendsGroup;
import com.friends.expenses.api.entity.GroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {

    List<Expense> findAllByCreatedByMemberAndGroup(GroupMember groupMember, FriendsGroup friendsGroup);
}
