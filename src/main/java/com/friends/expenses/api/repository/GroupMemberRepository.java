package com.friends.expenses.api.repository;

import com.friends.expenses.api.entity.GroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GroupMemberRepository extends JpaRepository<GroupMember, Long> {

    GroupMember findByEmail(String email);

}
