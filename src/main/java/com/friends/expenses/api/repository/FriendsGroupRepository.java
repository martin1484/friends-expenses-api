package com.friends.expenses.api.repository;

import com.friends.expenses.api.entity.FriendsGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendsGroupRepository extends JpaRepository<FriendsGroup, Long> {

    @Query(value = "select fg.* from friends_group fg \n" +
            "        inner join friends_group_members fgm on fg.id = fgm.friends_group_id \n" +
            "        inner join group_member gm on gm.id = fgm.members_id\n" +
            "        where gm.email = :email",nativeQuery = true)
    List<FriendsGroup> findAllGroupsByUserEmail(@Param("email") String email);

}
