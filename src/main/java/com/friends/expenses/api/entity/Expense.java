package com.friends.expenses.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.friends.expenses.api.dto.ExpenseDTO;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "expense")
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private BigDecimal amount;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private FriendsGroup group;

    private Date creationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private GroupMember createdByMember;

    public Expense() {
    }

    public Expense(Long id, String description, BigDecimal amount, FriendsGroup group, Date creationDate, GroupMember createdByMember) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.group = group;
        this.creationDate = creationDate;
        this.createdByMember = createdByMember;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public FriendsGroup getGroup() {
        return group;
    }

    public void setGroup(FriendsGroup group) {
        this.group = group;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public GroupMember getCreatedByMember() {
        return createdByMember;
    }

    public void setCreatedByMember(GroupMember createdByMember) {
        this.createdByMember = createdByMember;
    }

    public ExpenseDTO toDto() {
        return new ExpenseDTO(this.getId(),
                this.getAmount(),
                this.getDescription(),
                this.getCreatedByMember().getEmail(),
                this.getGroup().getId(),
                this.dateToStrDate());
    }

    public String dateToStrDate() {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(this.getCreationDate());
    }

}
