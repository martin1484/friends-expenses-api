package com.friends.expenses.api.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "friends_group")
public class FriendsGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "creation")
    private Date creationDate;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<GroupMember> members;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "group")
    private Set<Expense> expenses;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<GroupMember> admins;

    public FriendsGroup() {
    }

    public FriendsGroup(Long id, String name, Date creationDate, Set<GroupMember> members, Set<Expense> expenses, Set<GroupMember> admins) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.members = members;
        this.expenses = expenses;
        this.admins = admins;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<GroupMember> getMembers() {
        return members;
    }

    public void setMembers(Set<GroupMember> members) {
        this.members = members;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Set<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(Set<Expense> expenses) {
        this.expenses = expenses;
    }

    public Set<GroupMember> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<GroupMember> admins) {
        this.admins = admins;
    }
}
