package com.friends.expenses.api.service.impl;

import com.friends.expenses.api.dto.*;
import com.friends.expenses.api.entity.Expense;
import com.friends.expenses.api.entity.FriendsGroup;
import com.friends.expenses.api.entity.GroupMember;
import com.friends.expenses.api.exception.CalculationErrorException;
import com.friends.expenses.api.exception.FriendsGroupNotFoundException;
import com.friends.expenses.api.repository.ExpenseRepository;
import com.friends.expenses.api.repository.FriendsGroupRepository;
import com.friends.expenses.api.repository.GroupMemberRepository;
import com.friends.expenses.api.service.FriendsGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class FriendsGroupServiceImpl implements FriendsGroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FriendsGroupServiceImpl.class);

    @Autowired
    private FriendsGroupRepository friendsGroupRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Override
    public List<FriendsGroup> findAll() throws FriendsGroupNotFoundException {
        try {
            return this.friendsGroupRepository.findAll();
        } catch (Exception e) {
            String msg = "Error fetching all FriendsGroup";
            LOGGER.error(msg);
            throw new FriendsGroupNotFoundException(msg);
        }
    }

    @Override
    public FriendsGroup findById(Long id) throws FriendsGroupNotFoundException {
        try {
            Optional<FriendsGroup> friendsGroup = this.friendsGroupRepository.findById(id);
            return friendsGroup.orElse(null);
        } catch (Exception e) {
            String msg = "Error fetching FriendsGroup by id: "+id;
            LOGGER.error(msg);
            throw new FriendsGroupNotFoundException(msg);
        }
    }

    @Override
    public GroupDTO getGroupDTOById(Long id) throws FriendsGroupNotFoundException {
        try {
            Optional<FriendsGroup> friendsGroup = this.friendsGroupRepository.findById(id);
            return friendsGroup.map(this::createGroupDetail).orElse(null);
        } catch (Exception e) {
            String msg = "Error fetching FriendsGroup (getGroupDTOById) by id: "+id;
            LOGGER.error(msg);
            throw new FriendsGroupNotFoundException(msg);
        }
    }

    @Override
    public List<ResultCalculateDTO> calculate(Long id) throws CalculationErrorException {
        try {
            Optional<FriendsGroup> friendsGroup = this.friendsGroupRepository.findById(id);
            List<ResultCalculateDTO> resultCalculateDTOS = this.calculateResult(friendsGroup.get());
            return resultCalculateDTOS;
        } catch (Exception e) {
            String msg = "Error doing result calculations (calculate) for  groupId: "+id;
            LOGGER.error(msg);
            throw new CalculationErrorException(msg);
        }
    }

    @Override
    public FriendsGroup save(FriendsGroup friendsGroup) {
        try {
            Set<GroupMember> admins = friendsGroup.getAdmins();
            friendsGroup.setAdmins(new HashSet<>());
            friendsGroup.setCreationDate(new Date());
            Set<GroupMember> members = new HashSet<>();
            for (GroupMember member: friendsGroup.getMembers()) {
                GroupMember savedMember = this.groupMemberRepository.findByEmail(member.getEmail());
                if(savedMember != null){
                    members.add(savedMember);
                } else {
                    members.add(member);
                }
            }
            friendsGroup.setMembers(members);
            FriendsGroup savedGroup = this.friendsGroupRepository.save(friendsGroup);
            Set<GroupMember> newAdmins = new HashSet<>();
            for (GroupMember member: admins) {
                GroupMember savedMember = this.groupMemberRepository.findByEmail(member.getEmail());
                if(savedMember != null){
                    newAdmins.add(savedMember);
                } else {
                    newAdmins.add(member);
                }
            }
            savedGroup.setAdmins(newAdmins);
            return this.friendsGroupRepository.save(savedGroup);
        } catch (Exception e) {
            String msg = "Error saving group for group name: "+friendsGroup.getName();
            LOGGER.error(msg);
            throw e;
        }
    }

    @Override
    public FriendsGroup update(FriendsGroup friendsGroup) {
        try {
            Optional<FriendsGroup> optionalFriendsGroupToUpdate = this.friendsGroupRepository.findById(friendsGroup.getId());
            if (optionalFriendsGroupToUpdate.isPresent()){
                FriendsGroup groupToUpdate = optionalFriendsGroupToUpdate.get();
                groupToUpdate.setName(friendsGroup.getName());
                groupToUpdate.setMembers(friendsGroup.getMembers());
                FriendsGroup savedGroup = this.friendsGroupRepository.save(groupToUpdate);
                Set<GroupMember> newAdmins = new HashSet<>();
                for (GroupMember member: friendsGroup.getAdmins()) {
                    GroupMember savedMember = this.groupMemberRepository.findByEmail(member.getEmail());
                    if(savedMember != null){
                        newAdmins.add(savedMember);
                    } else {
                        newAdmins.add(member);
                    }
                }
                savedGroup.setAdmins(newAdmins);
                return this.friendsGroupRepository.save(savedGroup);
            } else {
                return null;
            }
        } catch (Exception e) {
            String msg = "Error updating group for group id: "+friendsGroup.getId();
            LOGGER.error(msg);
            throw e;
        }
    }

    @Override
    public List<FriendsGroup> findAllGroupsByUserEmail(String email) {
        return this.friendsGroupRepository.findAllGroupsByUserEmail(email);
    }

    private GroupDTO createGroupDetail(FriendsGroup friendsGroup){
        GroupDTO resultGroupDTO = new GroupDTO();
        BigDecimal total = BigDecimal.ZERO;
        List<MemberDTO> memberDTOList = new ArrayList<>();
        List<MemberDTO> adminDTOList = new ArrayList<>();
        resultGroupDTO.setId(friendsGroup.getId());
        resultGroupDTO.setName(friendsGroup.getName());
        Map<Long, BigDecimal> membersBalanceMap = this.calculateBalanceForMembers(new ArrayList<>(friendsGroup.getExpenses()));
        for (Map.Entry<Long, BigDecimal> entry : membersBalanceMap.entrySet()) {
            total = total.add(entry.getValue());
        }
        resultGroupDTO.setAmountPerMember(total.divide(new BigDecimal(friendsGroup.getMembers().size()),2, RoundingMode.HALF_DOWN));
        for (GroupMember member: friendsGroup.getMembers()) {
            BigDecimal memberTotalAmount = membersBalanceMap.get(member.getId()) != null ? membersBalanceMap.get(member.getId()) : BigDecimal.ZERO;
            BigDecimal memberDifferenceAmount = memberTotalAmount.subtract(resultGroupDTO.getAmountPerMember());
            MemberDTO dto = new MemberDTO(member.getId(),member.getFirstName(),member.getLastName(),member.getEmail(),
                    this.getExpensesForMemberAndGroup(member,friendsGroup),
                    memberTotalAmount,
                    memberDifferenceAmount);
            memberDTOList.add(dto);
        }
        resultGroupDTO.setGroupTotalAmount(total);
        resultGroupDTO.setMembers(memberDTOList);
        resultGroupDTO.setCreationDate(friendsGroup.getCreationDate());
        for (GroupMember admin : friendsGroup.getAdmins()) {
            adminDTOList.add(new MemberDTO(admin.getId(),admin.getFirstName(),admin.getLastName(),admin.getEmail()));
        }
        resultGroupDTO.setAdmins(adminDTOList);
        return resultGroupDTO;
    }

    private List<ResultCalculateDTO> calculateResult(FriendsGroup friendsGroup){
        ResultCalculateDTO resultCalculateDTO = new ResultCalculateDTO();
        GroupDTO resultGroupDTO = new GroupDTO();
        BigDecimal total = BigDecimal.ZERO;
        List<MemberDTO> memberDTOList = new ArrayList<>();
        resultGroupDTO.setId(friendsGroup.getId());
        resultGroupDTO.setName(friendsGroup.getName());
        Map<Long, BigDecimal> membersBalanceMap = this.calculateBalanceForMembers(new ArrayList<>(friendsGroup.getExpenses()));
        for (Map.Entry<Long, BigDecimal> entry : membersBalanceMap.entrySet()) {
            total = total.add(entry.getValue());
        }
        resultGroupDTO.setAmountPerMember(total.divide(new BigDecimal(friendsGroup.getMembers().size()),2, RoundingMode.HALF_DOWN));
        for (GroupMember member: friendsGroup.getMembers()) {
            BigDecimal memberTotalAmount = membersBalanceMap.get(member.getId()) != null ? membersBalanceMap.get(member.getId()) : BigDecimal.ZERO;
            BigDecimal memberDifferenceAmount = memberTotalAmount.subtract(resultGroupDTO.getAmountPerMember());
            MemberDTO dto = new MemberDTO(member.getId(),member.getFirstName(),member.getLastName(),member.getEmail(),
                    this.getExpensesForMemberAndGroup(member,friendsGroup),
                    memberTotalAmount,
                    memberDifferenceAmount);
            memberDTOList.add(dto);
        }
        resultGroupDTO.setGroupTotalAmount(total);
        resultGroupDTO.setMembers(memberDTOList);
        resultGroupDTO.setCreationDate(friendsGroup.getCreationDate());
        List<MemberDTO> positivesBalance = new ArrayList<>();
        List<MemberDTO> negativesBalance = new ArrayList<>();
        for (MemberDTO memberDTO : memberDTOList) {
            if (memberDTO.getMemberDifferenceAmount().compareTo(BigDecimal.ZERO) == 1){
                positivesBalance.add(memberDTO);
            }
            if (memberDTO.getMemberDifferenceAmount().compareTo(BigDecimal.ZERO) == -1){
                negativesBalance.add(memberDTO);
            }
        }
        positivesBalance.sort(Comparator.comparing(MemberDTO::getMemberDifferenceAmount).reversed());
        negativesBalance.sort(Comparator.comparing(MemberDTO::getMemberDifferenceAmount));
        List<ResultCalculateDTO> resultCalculateDTOList =  this.calculatePayments(positivesBalance,negativesBalance);
        return resultCalculateDTOList;
    }

    private Map<Long, BigDecimal> calculateBalanceForMembers(List<Expense> expenses){
        Map<Long, BigDecimal> resultMap = new HashMap<>();
        for (Expense e : expenses) {
            Long memberId = e.getCreatedByMember().getId();
            resultMap.merge(memberId, e.getAmount(), BigDecimal::add);
        }
        return resultMap;
    }

    private List<ExpenseDTO> getExpensesForMemberAndGroup(GroupMember groupMember, FriendsGroup friendsGroup){
        List<ExpenseDTO> expenseDTOS = new ArrayList<>();
        List<Expense> expenses = this.expenseRepository.findAllByCreatedByMemberAndGroup(groupMember, friendsGroup);
        for (Expense expense:expenses) {
            expenseDTOS.add(expense.toDto());
        }
        return expenseDTOS;
    }

    private List<ResultCalculateDTO> calculatePayments(List<MemberDTO> positivesBalance, List<MemberDTO> negativesBalance){
        List<ResultCalculateDTO> resultCalculateDTOS = new ArrayList<>();
        Map<Long,BigDecimal> negativesBalanceMap = new HashMap<>();
        Map<Long,BigDecimal> positivesBalanceMap = new HashMap<>();
        for (MemberDTO memberDTO:negativesBalance) {
            negativesBalanceMap.put(memberDTO.getId(),memberDTO.getMemberDifferenceAmount().negate());
        }
        for (MemberDTO memberDTO:positivesBalance) {
            positivesBalanceMap.put(memberDTO.getId(),memberDTO.getMemberDifferenceAmount());
        }
        int pos = 0;
        for (MemberDTO memberDTO : negativesBalance) {
            BigDecimal restToPayForDebtor = negativesBalanceMap.get(memberDTO.getId());
            ResultCalculateDTO resultCalculateDTO = new ResultCalculateDTO();
            resultCalculateDTO.setSourceMember(memberDTO.getBasicInfo());
            DestinationMemberDTO destinationMemberDTO = new DestinationMemberDTO();
            while(pos < positivesBalance.size() && validateZero(restToPayForDebtor).compareTo(BigDecimal.ZERO) > 0){
                BigDecimal restToPayToCreditor = positivesBalanceMap.get(positivesBalance.get(pos).getId());
                if(destinationMemberDTO.getDestinationMember() == null){
                    destinationMemberDTO.setDestinationMember(positivesBalance.get(pos).getBasicInfo());
                } else if(!destinationMemberDTO.getDestinationMember().getId().equals(positivesBalance.get(pos).getId())){
                    destinationMemberDTO.setDestinationMember(positivesBalance.get(pos).getBasicInfo());
                }
                if (validateZero(restToPayToCreditor).compareTo(BigDecimal.ZERO) > 0) {
                    if(restToPayForDebtor.compareTo(restToPayToCreditor) > 0){
                        destinationMemberDTO.setAmount(restToPayToCreditor);
                        restToPayForDebtor = restToPayForDebtor.subtract(restToPayToCreditor);
                        negativesBalanceMap.put(memberDTO.getId(),restToPayForDebtor);
                        positivesBalanceMap.put(positivesBalance.get(pos).getId(),BigDecimal.ZERO);
                        resultCalculateDTO.getDestinationMembers().add(destinationMemberDTO);
                    } else {
                        destinationMemberDTO.setAmount(restToPayForDebtor);
                        restToPayToCreditor = restToPayToCreditor.subtract(restToPayForDebtor);
                        positivesBalanceMap.put(positivesBalance.get(pos).getId(),restToPayToCreditor);
                        restToPayForDebtor = BigDecimal.ZERO;
                        negativesBalanceMap.put(memberDTO.getId(),restToPayForDebtor);
                        resultCalculateDTO.getDestinationMembers().add(destinationMemberDTO);
                    }
                } else {
                    pos++;
                    destinationMemberDTO = new DestinationMemberDTO();
                }
            }
            resultCalculateDTOS.add(resultCalculateDTO);
        }
        return resultCalculateDTOS;
    }

    private BigDecimal validateZero(BigDecimal amount) {
        if ( amount.compareTo(new BigDecimal("0.00")) > 0 && amount.compareTo(new BigDecimal("0.05")) < 0 ) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }
    }

}
