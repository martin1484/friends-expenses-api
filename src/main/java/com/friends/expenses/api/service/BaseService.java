package com.friends.expenses.api.service;

import java.util.List;

public interface BaseService<T> {

    List<T> findAll() throws Exception;

    T findById(Long id) throws Exception;

    T save(T t);

    T update(T t) throws Exception;
}
