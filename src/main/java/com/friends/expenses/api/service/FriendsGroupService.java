package com.friends.expenses.api.service;


import com.friends.expenses.api.dto.GroupDTO;
import com.friends.expenses.api.dto.ResultCalculateDTO;
import com.friends.expenses.api.entity.FriendsGroup;
import com.friends.expenses.api.exception.CalculationErrorException;
import com.friends.expenses.api.exception.FriendsGroupNotFoundException;

import java.util.List;

public interface FriendsGroupService extends BaseService<FriendsGroup>{

    List<FriendsGroup> findAllGroupsByUserEmail(String email);

    GroupDTO getGroupDTOById(Long id) throws FriendsGroupNotFoundException;

    List<ResultCalculateDTO> calculate(Long id) throws CalculationErrorException;
}
