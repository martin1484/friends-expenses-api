package com.friends.expenses.api.service.impl;

import com.friends.expenses.api.dto.ExpenseDTO;
import com.friends.expenses.api.entity.Expense;
import com.friends.expenses.api.entity.FriendsGroup;
import com.friends.expenses.api.entity.GroupMember;
import com.friends.expenses.api.exception.ExpenseNotFoundException;
import com.friends.expenses.api.repository.ExpenseRepository;
import com.friends.expenses.api.repository.FriendsGroupRepository;
import com.friends.expenses.api.repository.GroupMemberRepository;
import com.friends.expenses.api.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ExpenseServiceImpl implements BaseService<ExpenseDTO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseServiceImpl.class);

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private FriendsGroupRepository friendsGroupRepository;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Override
    public List<ExpenseDTO> findAll() {
        return null;
    }

    @Override
    public ExpenseDTO findById(Long id) {
        return null;
    }

    @Override
    public ExpenseDTO save(ExpenseDTO expenseDTO) {
        GroupMember member = this.groupMemberRepository.findByEmail(expenseDTO.getCreatedBy());
        FriendsGroup group = this.friendsGroupRepository.getOne(expenseDTO.getGroupId());
        Expense expense = new Expense();
        expense.setAmount(expenseDTO.getAmount());
        expense.setCreatedByMember(member);
        expense.setDescription(expenseDTO.getDescription());
        expense.setGroup(group);
        expense.setCreationDate(this.dateStringToDate(expenseDTO.getCreationDate()));
        Expense savedExpense = this.expenseRepository.save(expense);
        expenseDTO.setId(savedExpense.getId());
        return expenseDTO;
    }

    @Override
    public ExpenseDTO update(ExpenseDTO expenseDTO) throws ExpenseNotFoundException {
        Optional<Expense> expenseOptionalToUpdate = this.expenseRepository.findById(expenseDTO.getId());
        if (expenseOptionalToUpdate.isPresent()){
            Expense expenseToUpdate = expenseOptionalToUpdate.get();
            expenseToUpdate.setDescription(expenseDTO.getDescription());
            expenseToUpdate.setAmount(expenseDTO.getAmount());
            expenseToUpdate.setCreationDate(this.dateStringToDate(expenseDTO.getCreationDate()));
            return this.expenseRepository.save(expenseToUpdate).toDto();
        } else {
            String msg = "Expense with id: "+expenseDTO.getId()+" not found";
            LOGGER.error(msg);
            throw new ExpenseNotFoundException(msg);
        }
    }

    private Date dateStringToDate(String dateStr) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        //Parsing the given String to Date object
        Date date = null;
        try {
            date = formatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
