package com.friends.expenses.api.controller;

import com.friends.expenses.api.dto.ExpenseDTO;
import com.friends.expenses.api.entity.Expense;
import com.friends.expenses.api.repository.ExpenseRepository;
import com.friends.expenses.api.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/expense")
@CrossOrigin(origins = "*")
public class ExpenseController {

    @Autowired
    private BaseService<ExpenseDTO> expenseService;

    @Autowired
    private ExpenseRepository expenseRepository;

    @GetMapping
    public ResponseEntity<List<ExpenseDTO>> getAllExpenses() {
        try {
            List<Expense> expenses = this.expenseRepository.findAll();
            List<ExpenseDTO> result = new ArrayList<>();
            for (Expense expense : expenses) {
                result.add(new ExpenseDTO(expense.getId(),
                        expense.getAmount(),
                        expense.getDescription(),
                        expense.getCreatedByMember().getEmail(),
                        expense.getGroup().getId(),
                        expense.dateToStrDate()));
            }
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<ExpenseDTO> createExpense(@RequestBody ExpenseDTO expenseDTO) {
        try {
            return ResponseEntity.ok(this.expenseService.save(expenseDTO));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ExpenseDTO> updateExpense(@PathVariable(value = "id") Long id, @RequestBody ExpenseDTO expenseDTO) {
        try {
            expenseDTO.setId(id);
            return ResponseEntity.ok(this.expenseService.update(expenseDTO));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
