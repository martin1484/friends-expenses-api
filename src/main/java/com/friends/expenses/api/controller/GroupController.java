package com.friends.expenses.api.controller;

import com.friends.expenses.api.dto.GroupDTO;
import com.friends.expenses.api.dto.ResultCalculateDTO;
import com.friends.expenses.api.entity.FriendsGroup;
import com.friends.expenses.api.entity.GroupMember;
import com.friends.expenses.api.exception.CalculationErrorException;
import com.friends.expenses.api.service.FriendsGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/group")
@CrossOrigin(origins = "*")
public class GroupController {

    @Autowired
    private FriendsGroupService friendsGroupService;

    @GetMapping
    public ResponseEntity<List<FriendsGroup>> getAllGroups() {
        try {
            List result = this.friendsGroupService.findAll();
            if (result != null && !result.isEmpty()){
                return ResponseEntity.ok(result);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<GroupDTO> findById(@PathVariable(value = "id") Long id) {
        try {
            GroupDTO result = this.friendsGroupService.getGroupDTOById(id);
            if (result == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return ResponseEntity.ok(result);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/search")
    public ResponseEntity<List<FriendsGroup>> getAllGroupsByUserEmail(
            @RequestParam(value = "email") String email) {
        try {
            List result = this.friendsGroupService.findAllGroupsByUserEmail(email);
            if (result != null && !result.isEmpty()){
                return ResponseEntity.ok(result);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/calculate/{id}")
    public ResponseEntity<Object> calculate(@PathVariable(value = "id") Long id) {
        try {
            List<ResultCalculateDTO> resultCalculateDTOList = this.friendsGroupService.calculate(id);
            return ResponseEntity.ok(resultCalculateDTOList);
        } catch (CalculationErrorException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<FriendsGroup> createGroup(@RequestBody FriendsGroup friendsGroup) {
        try {
            return ResponseEntity.ok(this.friendsGroupService.save(friendsGroup));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<FriendsGroup> updateGroup(@PathVariable(value = "id") Long id,
                                              @RequestBody FriendsGroup friendsGroup) {
        FriendsGroup friendsGroupResult;
        try {
            friendsGroup.setId(id);
            return ResponseEntity.ok(this.friendsGroupService.update(friendsGroup));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
