package com.friends.expenses.api.util;

import com.friends.expenses.api.entity.Expense;
import com.friends.expenses.api.entity.FriendsGroup;
import com.friends.expenses.api.entity.GroupMember;
import com.friends.expenses.api.repository.ExpenseRepository;
import com.friends.expenses.api.repository.FriendsGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.util.*;

@Controller
public class DataLoader {

    @Autowired
    private FriendsGroupRepository friendsGroupRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    //method invoked during the startup
    //@PostConstruct
    public void loadData() {
        FriendsGroup friendsGroup = this.friendsGroupRepository.save(this.createGroup());
        List<GroupMember> members = new ArrayList<>(friendsGroup.getMembers());
        this.expenseRepository.save(new Expense(null, "Gasto 1 1", BigDecimal.TEN, friendsGroup, new Date(), members.get(0)));
        this.expenseRepository.save(new Expense(null, "Gasto 1 2", BigDecimal.TEN, friendsGroup, new Date(), members.get(0)));
        this.expenseRepository.save(new Expense(null, "Gasto 1 3", BigDecimal.TEN, friendsGroup, new Date(), members.get(0)));
    }

    private FriendsGroup createGroup(){
        return new FriendsGroup(null,"Grupo 1",new Date(), new HashSet<>(generateMembers()),new HashSet<>(),new HashSet<>());
    }

    private List<GroupMember> generateMembers() {
        List<GroupMember> members = new ArrayList<>();
        members.add(new GroupMember(null,"Martin","Beltramino","martin1484@gmail.com"));
        return members;
    }

}
