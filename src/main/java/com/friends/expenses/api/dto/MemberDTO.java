package com.friends.expenses.api.dto;

import java.math.BigDecimal;
import java.util.List;

public class MemberDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private List<ExpenseDTO> expenses;
    private BigDecimal memberTotalAmount;
    private BigDecimal memberDifferenceAmount;

    public MemberDTO() {
    }

    public MemberDTO(Long id, String firstName, String lastName, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public MemberDTO(Long id, String firstName, String lastName, String email, List<ExpenseDTO> expenses, BigDecimal memberTotalAmount, BigDecimal memberDifferenceAmount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.expenses = expenses;
        this.memberTotalAmount = memberTotalAmount;
        this.memberDifferenceAmount = memberDifferenceAmount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ExpenseDTO> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<ExpenseDTO> expenses) {
        this.expenses = expenses;
    }

    public BigDecimal getMemberTotalAmount() {
        return memberTotalAmount;
    }

    public void setMemberTotalAmount(BigDecimal memberTotalAmount) {
        this.memberTotalAmount = memberTotalAmount;
    }

    public BigDecimal getMemberDifferenceAmount() {
        return memberDifferenceAmount;
    }

    public void setMemberDifferenceAmount(BigDecimal memberDifferenceAmount) {
        this.memberDifferenceAmount = memberDifferenceAmount;
    }

    public BasicMemberInfoDTO getBasicInfo(){
        return new BasicMemberInfoDTO(this.id,this.firstName,this.lastName,this.email,this.memberTotalAmount,this.memberDifferenceAmount);
    }
}
