package com.friends.expenses.api.dto;

import java.util.ArrayList;
import java.util.List;

public class ResultCalculateDTO {

    private BasicMemberInfoDTO sourceMember;
    private List<DestinationMemberDTO> destinationMembers;

    public ResultCalculateDTO() {
        this.destinationMembers = new ArrayList<>();
    }

    public ResultCalculateDTO(BasicMemberInfoDTO sourceMember, List<DestinationMemberDTO> destinationMembers) {
        this.sourceMember = sourceMember;
        this.destinationMembers = destinationMembers;
    }

    public BasicMemberInfoDTO getSourceMember() {
        return sourceMember;
    }

    public void setSourceMember(BasicMemberInfoDTO sourceMember) {
        this.sourceMember = sourceMember;
    }

    public List<DestinationMemberDTO> getDestinationMembers() {
        return destinationMembers;
    }

    public void setDestinationMembers(List<DestinationMemberDTO> destinationMembers) {
        this.destinationMembers = destinationMembers;
    }
}


