package com.friends.expenses.api.dto;

import java.math.BigDecimal;

public class BasicMemberInfoDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private BigDecimal memberTotalAmount;
    private BigDecimal memberDifferenceAmount;

    public BasicMemberInfoDTO() {
    }

    public BasicMemberInfoDTO(Long id, String firstName, String lastName, String email, BigDecimal memberTotalAmount, BigDecimal memberDifferenceAmount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.memberTotalAmount = memberTotalAmount;
        this.memberDifferenceAmount = memberDifferenceAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getMemberTotalAmount() {
        return memberTotalAmount;
    }

    public void setMemberTotalAmount(BigDecimal memberTotalAmount) {
        this.memberTotalAmount = memberTotalAmount;
    }

    public BigDecimal getMemberDifferenceAmount() {
        return memberDifferenceAmount;
    }

    public void setMemberDifferenceAmount(BigDecimal memberDifferenceAmount) {
        this.memberDifferenceAmount = memberDifferenceAmount;
    }
}
