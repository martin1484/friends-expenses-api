package com.friends.expenses.api.dto;

import java.math.BigDecimal;

public class ExpenseDTO {

    private Long id;
    private BigDecimal amount;
    private String description;
    private String createdBy;
    private Long groupId;
    private String creationDate;

    public ExpenseDTO() {
    }

    public ExpenseDTO(Long id, BigDecimal amount, String description, String createdBy, Long groupId, String creationDate) {
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.createdBy = createdBy;
        this.groupId = groupId;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}
