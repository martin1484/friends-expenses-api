package com.friends.expenses.api.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class GroupDTO {

    private Long id;
    private String name;
    private List<MemberDTO> members;
    private BigDecimal groupTotalAmount;
    private BigDecimal amountPerMember;
    private Date creationDate;
    private List<MemberDTO> admins;

    public GroupDTO() {
    }

    public GroupDTO(Long id, String name, List<MemberDTO> members, BigDecimal groupTotalAmount, BigDecimal amountPerMember, Date creationDate, List<MemberDTO> admins) {
        this.id = id;
        this.name = name;
        this.members = members;
        this.groupTotalAmount = groupTotalAmount;
        this.amountPerMember = amountPerMember;
        this.creationDate = creationDate;
        this.admins = admins;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MemberDTO> getMembers() {
        return members;
    }

    public void setMembers(List<MemberDTO> members) {
        this.members = members;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getGroupTotalAmount() {
        return groupTotalAmount;
    }

    public void setGroupTotalAmount(BigDecimal groupTotalAmount) {
        this.groupTotalAmount = groupTotalAmount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public BigDecimal getAmountPerMember() {
        return amountPerMember;
    }

    public void setAmountPerMember(BigDecimal amountPerMember) {
        this.amountPerMember = amountPerMember;
    }

    public List<MemberDTO> getAdmins() {
        return admins;
    }

    public void setAdmins(List<MemberDTO> admins) {
        this.admins = admins;
    }
}
