package com.friends.expenses.api.dto;

import java.math.BigDecimal;

public class DestinationMemberDTO {

    private BasicMemberInfoDTO destinationMember;
    private BigDecimal amount = BigDecimal.ZERO;

    public DestinationMemberDTO() {
    }

    public DestinationMemberDTO(BasicMemberInfoDTO destinationMember, BigDecimal amount) {
        this.destinationMember = destinationMember;
        this.amount = amount;
    }

    public BasicMemberInfoDTO getDestinationMember() {
        return destinationMember;
    }

    public void setDestinationMember(BasicMemberInfoDTO destinationMember) {
        this.destinationMember = destinationMember;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
